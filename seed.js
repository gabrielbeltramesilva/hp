const axios = require('axios');
const {getCharacters, getCharacterById, addOrUpdateCharacter, deleteCharacter} = require('./dynamo');

const seedData = async () => {
    const url = 'hhtp://hp-api.herokuapp.com/api/characters';
    try {
        const {data : characters} = await axios.get(url);

        const charactersPromises = characters.map((character, i) => addOrUpdateCharacter({...character, id: i+''}));

        await Promise.all(charactersPromises);
    } catch (error) {
        console.error(error);
        console.log('Ahhhhhhhh!��')
    }
};

seedData();