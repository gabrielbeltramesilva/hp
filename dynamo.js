const AWS = require('aws-sdk');
require('dotenv').config();

AWS.config.update({
    region: process.env.AWS_DEFAULT_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

const dynamoClient = new AWS.DynamoDB.DocumentClient();
const TABLE_NAME = 'harrypotter-api';

const getCharacters = async () => {
    const params = {
        TableName: TABLE_NAME
    };

    return await dynamoClient.scan(params).promise();
};

const getCharacterById = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: {
            id
        }
    };

    return await dynamoClient.get(params).promise();
};

const addOrUpdateCharacter = async (caracter) => {
    const params = {
        TableName: TABLE_NAME,
        Item: caracter
    };

    return await dynamoClient.put(params).promise();
};

const deleteCharacter = async (id) => {
    const params = {
        TableName: TABLE_NAME,
        Key: {
            id
        }
    };

    return await dynamoClient.delete(params).promise();
};

module.exports = {
    dynamoClient, getCharacters, getCharacterById, addOrUpdateCharacter, deleteCharacter
}